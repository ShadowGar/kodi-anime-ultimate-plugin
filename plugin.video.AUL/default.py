import urllib
import urllib2
import re
import xbmcplugin
import xbmcgui
import string
import xbmcaddon
import xbmcvfs
import time
import os

from t0mm0.common.net import Net as net
from t0mm0.common.addon import Addon
from bs4 import BeautifulSoup as soup



#COMMON CACHE
try:
  import StorageServer
except:
  import storageserverdummy as StorageServer
cache = StorageServer.StorageServer('plugin.video.AUL')
local=xbmcaddon.Addon(id='plugin.video.AUL')
addon = Addon('plugin.video.AUL', sys.argv)

#DB TABLE
from sqlite3 import dbapi2 as database
db_dir = os.path.join(xbmc.translatePath("special://database"), 'AUL.db')
if not xbmcvfs.exists(os.path.dirname(db_dir)):
  xbmcvfs.mkdirs(os.path.dirname(db_dir))
db = database.connect(db_dir)
db.execute('CREATE TABLE IF NOT EXISTS info (name UNIQUE,meta,url)')
db.commit()
db.close()

 
def meta(name,url):
  db = database.connect( db_dir )
  cur = db.cursor()
  try: cur.execute('SELECT * FROM info WHERE name = "%s"' %name)
  except:pass
  cached = cur.fetchone()
  if cached:
    db.close()
    return cached[1].encode('utf-8')
  else:
    addon.log('No cached response. Requesting from internet')
    try: data = net().http_GET(url).content
    except:pass
    try:
      blob=re.compile('<a href="/genres/.+?/">(.+?)</a>').findall(re.compile('<td class="tdhead">Genres</td><td>(.+?)</td>').findall(data)[0])
      genres=','.join(blob[:3])
      if not genres: genres='[COLOR red]No genres available.[/COLOR]'
    except: genres='[COLOR red]No genres available.[/COLOR]'
    try:
      desc=re.compile('<p>(.+?)</p>',re.DOTALL).findall(data)[0]
      if 'AnimeUltima' in desc: desc=name
    except: desc = 'No Plot Synopsis found, sorry.'
    try: image=re.compile('<img class="anime-pic" src="(.+?)"').findall(data)[0]
    except: image=''
    try: eps='%s Episodes'%re.compile('<td class="tdhead">Episodes</td><td>(.+?)</td>').findall(data)[0]
    except: eps='? Episodes.'
    try: status='[B][COLOR yellow]STATUS:[/COLOR][/B] [B][COLOR red]%s[/COLOR][/B]'%re.compile('<td class="tdhead">Status</td><td>(.+?)</td>').findall(data)[0]
    except: status='[B][COLOR yellow]GENRES:[/COLOR][/B] [B][COLOR red]Completed[/COLOR][/B][CR].'
    meta = str((genres,desc,eps,status,image))
    sql = 'INSERT OR REPLACE INTO info (name,meta,url) VALUES(?,?,?)'
    db.text_factory = str
    cur.execute(sql, (name, meta, url))
    db.commit()
    db.close()
    return meta


def CLEAN(text):
  command={'[B':'','[/COLOR':'','[COLOR green':'','[COLOR yellow':'',
           '[COLOR red':'','[COLOR orange':'',"'":'','(':'',')':'','\r':'','\n':'',
           '[COLOR blue':'','[COLOR purple':'','&#038;':'&','&#8217;':"`","'":'',
           ']':'','Subbed':'','Dubbed':'','mini series':'','&#9734;':'-',
           '&amp;':'&','~':'','&#9733;':' ','[/B':'','</span>':'','[h':'h',
           '[b':'','[/color':'','[color green':'','[/b':'','<b>':'','</b>':'','<p>':'','</p>':'','\\':'','//':'','&#363;':'uu'}
  regex = re.compile("|".join(map(re.escape, command.keys())))
  return regex.sub(lambda mo: command[mo.group(0)], text)

  
def CATS():
  addDir("[B][COLOR green]FULL LISTING[/COLOR][/B]",'http://www.animeultima.tv/watch-anime/',12,"%s/art/full.png"%local.getAddonInfo("path"),1,False)
  addDir("[B][COLOR green]A-Z LISTING[/COLOR][/B]",'http://www.animeultima.tv/watch-anime/',5,"%s/art/atoz.png"%local.getAddonInfo("path"),1,False)
  addDir("[B][COLOR green]ONGOING ANIME[/COLOR][/B]",'http://www.animeultima.tv/',6,"%s/art/plus.png"%local.getAddonInfo("path"),1,False)
  addDir("[B][COLOR green]ANIME MOVIES[/COLOR][/B]",'http://www.animeultima.tv/watch-anime-movies/',12,"%s/art/movies.png"%local.getAddonInfo("path"),1,False)
  addDir("[B][COLOR green]LIVE ACTION[/COLOR][/B]",'http://www.animeultima.tv/watch-live-action-anime/',12,"%s/art/live.png"%local.getAddonInfo("path"),1,False)
  addDir("[B][COLOR green]FAVOURITES[/COLOR][/B]",'http://www.animeultima.tv/watch-live-action-anime/',9,"%s/art/favourites.png"%local.getAddonInfo("path"),1,False)
  addSpecial('DOWNLOAD METAPACK')
               
def AtoZ():
        addDir('0-9','http://www.animeultima.tv/watch-anime/#0-9',1,"%s/art/0to9.png"%local.getAddonInfo("path"),1,False)
        for i in string.ascii_uppercase:
                addDir(i,'%s#%s'%('http://www.animeultima.tv/watch-anime/',i),1,"%s/art/Alphabet-%s.png"%(local.getAddonInfo("path"),i),26,0)
def ONG(url):
        links=re.compile('<a title=".+?" href="(.+?)">(.+?)</a>.+?<span class=".+?">(.+?)<',re.DOTALL).findall(net().http_GET(url).content)
        for url,name,voice in links:
                addDir("[B][COLOR green]%s[/COLOR][/B]" % CLEAN(name).upper(),url,2,'',len(links),True)
                
                
def INDEX(url,name):
        Alpha=[]
        links=re.compile('<li><a href="(.+?)" title=".+?">(.+?)</a></li>').findall(net().http_GET(url).content)
        for i in range(0,len(links)):
                if links[i][1][:1].isdigit() == True and name == "0-9":
                        Alpha.append(links[i])
                elif links[i][1][:1] == name:
                        Alpha.append(links[i])
        for urls,name in Alpha:
                addDir("[B][COLOR green]%s[/COLOR][/B]" % CLEAN(name).upper(),urls,2,'',len(Alpha),True)

def FAVS():
  saved_favs = cache.get('favourites_')
  if not saved_favs:
    xbmc.executebuiltin("XBMC.Notification([B][COLOR orange]Anime Ultima[/COLOR][/B],[B]You have no favourites saved.[/B],5000,"")");CATS()
  if saved_favs == '[]':
    xbmc.executebuiltin("XBMC.Notification([B][COLOR orange]Anime Ultima[/COLOR][/B],[B]You have no favourites saved.[/B],5000,"")");CATS()
  if saved_favs:
    favs = sorted(eval(saved_favs), key=lambda fav: fav[1])
    try:
      for fav in favs:
        addDir("[B][COLOR green]%s[/COLOR][/B]" % CLEAN(fav[0]).upper(),fav[1],2,'',len(favs),True)
    except: xbmc.executebuiltin("XBMC.Notification([B][COLOR orange]Anime Ultima[/COLOR][/B],[B]You have no favourites saved.[/B],5000,"")");CATS()
  
def add_favourite(name,url):
    saved_favs = cache.get('favourites_')
    favs = []
    if saved_favs:
        favs = eval(saved_favs)
        if favs:
            if (name,url) in favs:
                xbmc.executebuiltin("XBMC.Notification([B][COLOR orange]"+name.upper()+"[/COLOR][/B],[B] Already in your Favourites[/B],5000,"")")
                return

    favs.append((name,url))
    cache.set('favourites_', str(favs))
    xbmc.executebuiltin("XBMC.Notification([B][COLOR orange]"+name.upper()+"[/COLOR][/B],[B] Added to Favourites[/B],5000,"")")

def remove_favourite(name,url):
  saved_favs = cache.get('favourites_')
  if saved_favs:
    favs = eval(saved_favs)
    favs.remove((name,url))
    cache.set('favourites_', str(favs))
    xbmc.executebuiltin("XBMC.Container.Refresh")
    xbmc.executebuiltin("XBMC.Notification([B][COLOR orange]"+name.upper()+"[/COLOR][/B],[B] Removed from Favourites[/B],5000,"")")

def FULL(url):
        up=re.compile('<li><a href="http:/\/\www.+?animeultima..+?/(.+?)" title=".+?">(.+?)</a></li>').findall(net().http_GET(url).content)
        for url,name in up:
            addDir("[B][COLOR green]%s[/COLOR][/B]" % CLEAN(name).upper(),"http://www.animeultima.tv/%s"%url,2,'',len(up),True)

def EPISODES(name,url):
        try: thumb=re.compile('<img class="anime-pic" src="(.+?)"').findall(net().http_GET(url).content)[0]
        except: thumb = ''
        eps=re.compile('<td class="epnum">(.+?)</td><td class="title"><a href="(.+?)">(.+?)</a></td><td class=.+?</td><td class="td-lang-subbed">').findall(net().http_GET(url).content)
        if not eps: eps=re.compile('<td class="epnum">(.+?)</td><td class="title"><a href="(.+?)">(.+?)</a></td><td class=.+?</td><td class="td-lang-dubbed">').findall(net().http_GET(url).content)
        if not eps: addDir('[B][COLOR red]NO SOURCE FOUND REPORT ON WEBSITE.[/COLOR][/B]',url,3,thumb,1,0)
        for epnum,url,namey in eps:
          addDir("%s:- [B][COLOR orange]%s[/COLOR]-[UPPERCASE]%s[/UPPERCASE][/B]"%(epnum,name.replace('Subbed','').replace('Dubbed','').replace('[/B]',''),namey),url,3,thumb,len(eps),0)
            
def LINKS(url,name):
    links=[]
    data=net().http_GET(url).content
    list=re.compile('<a rel="nofollow" href="(.+?)">').findall(data)
    try:
      title=re.compile('<strong>Video Site:</strong>(.+?)<br />').findall(data)[0].strip().upper()
      language=re.compile('<strong>Language:</strong>(.+?)<div class="report-video">').findall(data)[0].strip().lower()
      links.append((title,'%s-english-%s'%(url[:-1],language)))
    except: addDir('[B][COLOR red]NO SOURCE FOUND REPORT ON WEBSITE.[/COLOR][/B]',url,3,'',1,0)
    for url in list:
            links.append((url.split('-')[-1].replace('/','').upper(),'http://www.animeultima.tv%s'%url))
    if local.getSetting('Allow Yourupload Links') == "True":
      names = ['AUENGINE','MP4UPLOAD','VIDEOWEED','NOVAMOV','DAILYMOTION','UPLOADC','VIDEONEST','YOURUPLOAD']
    else: names = ['AUENGINE','MP4UPLOAD','VIDEOWEED','NOVAMOV','DAILYMOTION','UPLOADC','VIDEONEST']
    for name,url in links:
      if name in str(names) and 'dubbed' in url:
        VIDS('[B]%s[/B] - [COLOR red]DUBBED[/COLOR]'%name,url)
      if name in str(names) and 'subbed' in url:
        VIDS('[B]%s[/B] - [COLOR yellow]SUBBED[/COLOR]'%name,url)
          
def parts(name,url):
  parts=re.compile('rel="part-(\d+)" class=".+?">(.+?)</a>').findall(net().http_GET(url).content)
  if parts:
        for id,ident in parts:
          VIDS('%s-%s'%(name.upper(),ident.upper()),re.sub('mirror-\d+-','mirror-'+id+'-',url))
				
def VIDS(name,url):
  vidlinks=[]
  if 'AUENGINE' in name:
    try:
      Auengine=re.compile('<div class="player-embed" id="pembed"><iframe src="(.+?)".+?').findall(net().http_GET(url).content)[0]
      Au=urllib.unquote(re.compile("url: '(.+?)'").findall(net().http_GET(Auengine).content)[-1])
      vidlinks.append((name,Au,'http://static.cdn.animeultima.tv/images/logo.png'))
    except: pass
       
  if 'MP4UPLOAD' in url:
    try:
      mp4upload=re.compile('<iframe title="MP4Upload" type="text/html" frameborder="0" scrolling="no" width=".+?" height=".+?" src="http://mp4upload.com/embed-.+?-650x370.html">').findall(net().http_GET(url).content)[0]
      mp4=re.compile("'file': '(.+?)'").findall(net().http_GET(mp4upload).content)[0]
      vidlinks((name,mp4,'http://www.mp4upload.com/images/logo.png'))
    except:pass
	
    
  if 'VIDEOWEED' in name:
    try:
      videoweed="http://www.videoweed.es/file/%s"%re.compile('<div class="player-embed" id="pembed".+?iframe src="http://embed.+?videoweed.+?es/embed.+?v=(.+?)&.+?" frameborder="0"').findall(net().http_GET(url).content)[0]
      a=net().http_GET(videoweed).content
      key=re.compile('flashvars.filekey="(.+?)"').findall(str(a))[0].replace('.','%2e').replace('-','%2d')
      b=net().http_GET('http://www.videoweed.es/api/player.api.php?pass=undefined&codes=1&user=undefined&file=%s&key=%s'%(videoweed.replace('http://www.videoweed.es/file/',''),key)).content
      vid=re.compile('url=(.+?)&title').findall(b)[0]
      vidlinks.append((name,vid,'http://www.modmyscript.com/product_images/i/558/videoweed__69085_zoom.jpg'))
    except: pass
    
  if 'NOVAMOV' in name:
    try:
      nova="http://www.novamov.com/video/%s"%re.compile('<div class="player-embed" id="pembed".+?iframe src="http://embed.+?novamov.+?com/embed.+?v=(.+?)&.+?" frameborder="0"').findall(net().http_GET(url).content)[0]
      a=net().http_GET(nova).content
      key=re.compile('flashvars.filekey="(.+?)"').findall(str(a))[0].replace('.','%2e').replace('-','%2d')
      b=net().http_GET('http://www.novamov.com/api/player.api.php?pass=undefined&codes=1&user=undefined&file=%s&key=%s'%(nova.replace('http://www.novamov.com/video/',''),key)).content
      vid=re.compile('url=(.+?)&title').findall(b)[0]
      vidlinks.append((name,vid,'http://www.userlogos.org/files/logos/Sanjay/novamov_trans_refl_glow_01.png'))
    except: pass
    
  if 'DAILYMOTION' in url:
    try:
      daily=re.compile('<div class="player-embed" id="pembed"><embed src="(.+?)" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width=".+?" height=".+?" wmode="transparent"',re.DOTALL).findall(net().http_GET(url).content)[0]
      redirect = urllib.unquote(net().http_GET(net().http_GET(daily).get_url().replace('swf/','')).content)
      dail = urllib.unquote(re.compile('"video_url":"(.+?)"',re.DOTALL).findall(redirect)[0])
      vidlinks.append((name,dail,'http://static1.dmcdn.net/images/dailymotion-logo-ogtag.png.v26907a7cb2bfd44b9'))
    except: pass
           
  if 'YOURUPLOAD' in name:
    try:
      yu=re.compile('<iframe title="YourUpload" type="text/html" frameborder=".+?" scrolling="no" width=".+?" height=".+?" src="(.+?)">').findall(net().http_GET(url).content)[0]
      data2 = urllib.unquote(net().http_GET(yu).content)
      yupload='http://f11'+re.compile('<meta property="og:video" content="http://f\d{2}(.+?)&amp;file').findall(data2)[0]
      yupload='http://f11'+re.compile('file=http://f\d{2}(.+?)&file_size').findall(data2)[0]
      vidlinks.append((name,yupload,'http://www.yourupload.com/theme/images/logo.png'))
    except: pass
     
  if 'UPLOADC' in name:
    try:
      uploadc=re.compile('<iframe src="(.+?)" frameborder="0"',re.DOTALL).findall(net().http_GET(url).content)[0]
      c = re.compile("'file','(.+?)'",re.DOTALL).findall(net().http_GET(c).content)[0]
      vidlinks.append((name,c,'http://www.uploadc.com/img/uploadc.com.jpg'))
    except: pass
 
  if 'VIDEONEST' in name:
    try:
      nest=re.compile('<iframe title="VideoNest" type="text/html" frameborder="0" scrolling="no" width=".+?" height=".+?" src="(.+?)">" frameborder="0"',re.DOTALL).findall(net().http_GET(url).content)[0]
      n = re.compile("'file','(.+?)'",re.DOTALL).findall(net().http_GET(n).content)[0]
      vidlinks.append((name,n,'http://www.videonest.net/images/logo_2_2.png'))
    except: pass

  if vidlinks:
    for name,url,image in vidlinks:
      addLink(name,url,image,len(vidlinks))
    
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param


def addLink(name,url,thumbnail,items):
  liz=xbmcgui.ListItem(label=name, iconImage="DefaultVideo.png", thumbnailImage=thumbnail)
  liz.setProperty('IsPlayable', 'true')
  liz.setInfo( type="Video", infoLabels={ "Title": name  } )
  xbmcplugin.addDirectoryItem(int(sys.argv[1]),url,liz,isFolder=False,totalItems=items)

def addDir(name,url,mode,iconimage,items,meta_data):
  if meta_data==True:
    grab=eval(meta(name,url))#genres,desc,eps,status,image
    liz=xbmcgui.ListItem('%s - %s'%(name,grab[2]), iconImage="DefaultFolder.png", thumbnailImage=grab[-1])
    liz.setInfo( type="Video", infoLabels={ "Title" : name, "Plot" : '%s[CR][B][COLOR yellow]GENRES:[/COLOR][/B] [B][COLOR red]%s[/COLOR][/B][CR]%s'%(grab[3],grab[0],CLEAN(grab[1]))})
  else:
    liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
    liz.setInfo( type="Video", infoLabels= { "Title": name } )

  liz.addContextMenuItems([('[B][COLOR green]ADD[/COLOR][/B] ~  [B]To Favourites[/B]',"XBMC.RunPlugin(%s?mode=%s&name=%s&url=%s)"%(sys.argv[0],10,CLEAN(name),url)),
                           ('[B][COLOR green]REMOVE[/COLOR][/B] ~ [B]From Favourites[/B]',"XBMC.RunPlugin(%s?mode=%s&name=%s&url=%s)"%(sys.argv[0],11,CLEAN(name),url))])
  
  xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_LABEL)
  xbmcplugin.addDirectoryItem(int(sys.argv[1]),'%s?url=%s&mode=%s&name=%s'%(sys.argv[0],urllib.quote_plus(url),mode,urllib.quote_plus(name)),liz,totalItems=items,isFolder=True)
  xbmc.executebuiltin('Container.SetViewMode('+addon.get_setting('Category View')+')') 
  
def addSpecial(name):
  liz=xbmcgui.ListItem(name,iconImage="DefaultFolder.png", thumbnailImage='http://gamepsvita.com/wp-content/themes/i3theme-1-8-classic/i3theme-1-8-classic/images/download.png')
  xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=sys.argv[0]+"?url=http://download.something.com&mode=13&name=%s"%name,isFolder=False,listitem=liz)

def _pbhook(numblocks, blocksize, filesize, dp, start_time):
        try: 
            percent = min(numblocks * blocksize * 100 / filesize, 100) 
            currently_downloaded = float(numblocks) * blocksize / (1024 * 1024) 
            kbps_speed = numblocks * blocksize / (time.time() - start_time) 
            if kbps_speed > 0: 
                eta = (filesize - numblocks * blocksize) / kbps_speed 
            else: 
                eta = 0 
            kbps_speed = kbps_speed / 1024 
            total = float(filesize) / (1024 * 1024) 
            mbs = '%.02f MB of %.02f MB' % (currently_downloaded, total) 
            e = 'Speed: %.02f Kb/s ' % kbps_speed 
            e += 'ETA: %02d:%02d' % divmod(eta, 60) 
            dp.update(percent, mbs, e)
        except: 
            percent = 100 
            dp.update(percent) 
        if dp.iscanceled(): 
            dp.close() 

def download():
  if os.path.isfile(db_dir):
    dialog = xbmcgui.Dialog()
    ok = dialog.ok('[B][COLOR red]Meta Notice.[/COLOR][/B]',
                   'You have some meta already',
                   'Do you wish a [B]FULL INSTALL ?[/B]',
                   'This will remove previous meta.')
    if ok==True:
      os.remove(db_dir)
      download()
    else: CATS()
  else:
    dialog = xbmcgui.Dialog()
    ok = dialog.ok('[B][COLOR red]Meta Notice.[/COLOR][/B]',
                   'Do you wish to install [B]AUL META[/B]',
                   'A [B]FULL INSTALL[/B] is 2mb ?')
    if ok==True: pass
    else: CATS()
    
  print 'Downloading AUL.DB'
  print 'URL: %s' % 'https://www.dropbox.com/s/0h6rehsjw49w0at/AUL.db?dl=1'
  dp = xbmcgui.DialogProgress()
  dp.create('Downloading', '', 'ANIME ULTIMATE METAPACK')
  start_time = time.time()
  if os.path.isfile(db_dir):
    if os.path.getsize(db_dir) == 104098816:
      addon.show_small_popup('[B][COLOR red]METAPACK INSTALLED[/B][/COLOR][B][COLOR yellow] ;)[/COLOR][/B]','', delay=500, image='')
    else: addon.show_small_popup('[B][COLOR red]Meta already installed.[/COLOR][/B]','', delay=500, image='')
    return True
  else:
    try:
      urllib.urlretrieve('https://www.dropbox.com/s/0h6rehsjw49w0at/AUL.db?dl=1', db_dir, lambda nb, bs, fs: _pbhook(nb, bs, fs, dp, start_time)) 
    except:
      addon.show_small_popup('An ERROR has occured', 'Please try again.', delay=500, image='')
      if os.path.isfile(db_dir):
        xbmcvfs.delete(db_dir)
          
     
params=get_params()
url=None
name=None
mode=None
try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass

print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)

if mode==None or url==None or len(url)<1:
  if local.getSetting('Check update') == 'True':
    dialog = xbmcgui.Dialog()
    ok = dialog.ok('[B][COLOR red]Meta Notice.[/B][/COLOR]',
                   'You have either updated or it`s a first run.',
                   'Do you wish a [B]FULL INSTALL [/B] of meta_data ?',
                   'This will remove ANY previous meta.')
    if ok==True:
      download()
      local.setSetting(id='Check update', value='False')
    else: pass
  CATS()
elif mode==1:
        INDEX(url,name)
elif mode==2:
        EPISODES(name,url)
elif mode==3:
        LINKS(url,name)
elif mode==5:
        AtoZ()
elif mode==6:
        ONG(url)
elif mode==7:
        VIDS(name,url)
elif mode==8:
        ALT(url)
elif mode==9:
        FAVS()
elif mode==10:
        add_favourite(name,url)
elif mode==11:
        remove_favourite(name,url)
elif mode==12:
        FULL(url)
elif mode==13:
        download()

xbmcplugin.endOfDirectory(int(sys.argv[1]),succeeded=True)

